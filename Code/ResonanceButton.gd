extends Button

var magnetic_angle = 0
var frequency_center = 2.87
var frequency_range = 0.2
var num_steps = 101
var num_runs = 25
var uwave_power = -15

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _on_TabContainer_tab_selected(tab):
	if tab == 1:
		print(magnetic_angle)


func _on_ResonanceButton_pressed():
	$"../FreqCenter".show()
	$"../FreqRange".show()
	$"../NumSteps".show()
	$"../NumRuns".show()
	$"../UwavePower".show()
