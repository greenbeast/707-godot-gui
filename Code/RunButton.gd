extends Button

var magnetic_angle 
var frequency_center
var frequency_range
var num_steps
var num_runs
var uwave_power

func _on_RunButton_pressed():
	magnetic_angle = $"../MagAngle".get_text()
	magnetic_angle = int(magnetic_angle)
	frequency_center = $"../FreqCenter".get_text()
	frequency_range = $"../FreqRange".get_text()
	num_steps = $"../NumSteps".get_text()
	num_runs = $"../NumRuns".get_text()
	uwave_power = $"../UwavePower".get_text()
	if magnetic_angle > 180 or magnetic_angle < 0:
		print("Magnetic angle has to be between 0-180")
		$"../Exception".show()
	else:
		$"../ConfirmationExp1".show()
