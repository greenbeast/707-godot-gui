import argparse

def testing():
    print('Hello World')
    return 5

parser = argparse.ArgumentParser()
parser.add_argument('--name', type=str, help='name of the user')


args = parser.parse_args()

print(args.name)

match args.name:
    case 'Test':
        print('Test working')
        testing()
    case 'accumulate':
        print('accumulate')
    case 'name':
        print('name')
    case _:
        print('default')

"""
I run the script with the following command:

python python_test.py 1 2 3 4 5 --name "John Doe"
"""
