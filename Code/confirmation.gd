extends AcceptDialog


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	self.dialog_text = "Scan running! Plot will appear in another winodw!"


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Confirmation_confirmed():
	$"../XCoordinate".show()
	$"../YCoordinate".show()
	$"../ZCoordinate".show()
	$"../Confirmation".show()
	$"../image-size".show()
	self.hide()
	$"../OptimizeButton".show()
	$"../Scan Button".show()
