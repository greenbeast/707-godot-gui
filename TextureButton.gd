extends TextureButton

func _ready():
	pass # Replace with function body.

func _on_TextureButton_pressed():
	var output = [] # Output of our python code
	var coordinates = $"../coordinates-box".get_text() # Gets the coordinates from the text box
	var first = false # Just used to create a \n for the first element of our python output
	var selected_experiment_id = $"../experiment-number".get_selected_id() # Gets the id of the drop-down menu
	var selected_experiment

	# The match case here is just taking out ID and converting that to an experiment number
	match (selected_experiment_id):
		0:
			selected_experiment = "Experiment 0"
		1:
			selected_experiment = "Experiment 1"
		2:
			selected_experiment = "Experiment 2"
		3:
			selected_experiment = "Experiment 3"

	# This runs the python script with the first option being the command, then the array being
	# arguments and output being out output of the script
	OS.execute("python", ["test.py", "-o", selected_experiment], true, output, true, true)

	# Cycle through outputs and print out each element
	for i in output:
		if first == false:
			first = true
			print("\n")
		print(i)
		
	print(coordinates)
