import argparse


"""
This is a test script to test the argparse module
with Godot Engine.
"""

# Initialize parser
parser = argparse.ArgumentParser()

# Adding optional argument
parser.add_argument("-n", "--Name", help = "Show Output")

# Read arguments from command line
args = parser.parse_args()

def main():
    print("Test")

if args.Name != 0:
    main()
    print(f"Displaying Output as: {args.Name}")
    # print("Displaying Output as: % s" % args.Output)

